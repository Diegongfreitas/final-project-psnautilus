#!/usr/bin/env python3

import numpy as np
import cv2
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import matplotlib.pyplot as plt

class Fish_camera():
    '''classe que processa a câmera para retornar uma imagem filtrada onde é preto o que não é verde e branco o que é'''
    def __init__(self):
        '''Construtora inicializa todos os Publishers e Subscribers e variaveis definidas nos callbacks'''
        self.img = np.zeros((1000, 1000))
        self.bridge = CvBridge()
        self.pub = rospy.Publisher('processed_image', Image, queue_size=10)
        self.sub = rospy.Subscriber('camera/image_raw', Image, self.callback)
        self.light_green = (55,150,100) #valor mínimo encontrado experimentalmente
        self.dark_green = (65,255,180) #valor máximo encontrado experimentalmente
    
    def callback(self, msg):

        self.img = self.bridge.imgmsg_to_cv2(msg, "bgr8")#transforma a msg passada do tipo Image para numpy.ndarray, mais facilmente utilizável

        hsv_img = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)#transforma o espaço de cores de BGR para HSV

        mask = cv2.inRange(hsv_img, self.light_green, self.dark_green)#Cria a máscara que filtra somente o verde

        result = cv2.bitwise_and(self.img, self.img, mask=mask)#aplica a máscara na imagem
        
        result = self.bridge.cv2_to_imgmsg(mask, "mono8")#transforma o que é verde em branco

        self.pub.publish(result)#publica o resultado

       

if __name__ == '__main__':
    try:

        rospy.init_node('image_processor', anonymous=True)
        s = Fish_camera()
        rospy.spin()

    except rospy.ROSInterruptException:
        pass