import cv2
from cv_bridge import CvBridge
import matplotlib.pyplot as plt

# dark_green = (125,95,30)
# light_green = (115,105,50)
dark_green = (60,160,160)
light_green = (60,255,102)
bridge = CvBridge()

img = cv2.imread('./camsnap3.png')

hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

mask = cv2.inRange(img, dark_green, light_green)

img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

result = cv2.bitwise_and(img, img, mask=mask)

# plt.hsv()
plt.imshow(mask, cmap="gray")
# plt.imshow(img)
plt.show()