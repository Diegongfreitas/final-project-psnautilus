#!/usr/bin/env python3

import rospy
import numpy as np
from geometry_msgs.msg import Twist, PointStamped #tipo de mensagem usada nos tópicos /cmd_vel e /sonar
from nav_msgs.msg import Odometry #tipo de mensagem usada no tópico /odom
from std_msgs.msg import Bool, Float64
from tf.transformations import euler_from_quaternion, quaternion_from_euler

class Fish_mover():
    '''Classe de movimentação geral do carrinho, faz uma série de comunicações através dos métodos de Publisher e Subscriber com 
        todos os tópicos relacionados a movimentação e localização do carrinho, incluindos os criados no meio do processo. '''
    def __init__(self):
        '''Construtora inicializa todos os Publishers e Subscribers e variaveis definidas nos callbacks'''
        self.sub_odom = rospy.Subscriber("odom", Odometry, self.callback_odom)
        self.sub_sonar = rospy.Subscriber("sonar_data", PointStamped, self.callback_sonar)
        self.sub_validator = rospy.Subscriber("robot_validator", Bool, self.callback_validator)
        self.sub_camera = rospy.Subscriber("position_on_camera", Float64, self.callback_camera)

        self.pub_marlin_vel = rospy.Publisher("cmd_vel", Twist, queue_size=10)

        self.is_seeing = False
        self.position = 0
        
        self.sonar_data = PointStamped()
        self.odom = Odometry()
        self.reldist_relframe = np.zeros((3,1))

    def callback_odom(self, msg):   
        '''Função chamada pelo subscriber self.sub_odom para salvar os dados da Odometria'''
        self.odom = msg     

    def callback_sonar(self, msg):  
        '''Função chamada pelo subscriber self.sub_sonar para salvar os  
        dados da distância relativa entre os carrinhos no frame global'''
        self.sonar_data = msg       
    
    def callback_validator(self, msg):   
        '''Função chamada pelo subscriber self.sub_validator para salvar a mensagem Booleana
        #que informa se a camera enxerga ou não o bastão verde do Nemo'''
        self.is_seeing = msg.data      
    
    def callback_camera(self, msg): 
        '''Função chamada pelo subscriber self.sub_camera para salvar o dado da distância do bastão
        verde até o centro da câmera'''
        self.position = msg.data    
    
    def set_reldist_relframe(self):
        '''Função de troca de frame de referência, utiliza uma matriz de rotação para calcular a distância
        relativa, a partir dos dados gerados pelo sonar, entre os dois carrinhos, mas dessa vez em um frame relativo e não global.
        Essa função da ao Marlin o poder de se movimentar em direção ao Nemo mesmo que apenas possua informações advindas do sonar'''
        reldist_absframe = np.array([[self.sonar_data.point.x, self.sonar_data.point.y, self.sonar_data.point.z]]).T

        roll,pitch,yaw = euler_from_quaternion([self.odom.pose.pose.orientation.x,self.odom.pose.pose.orientation.y,
                                                self.odom.pose.pose.orientation.z,self.odom.pose.pose.orientation.w])

        M = np.array([
            [np.cos(-yaw), -np.sin(-yaw), 0],
            [np.sin(-yaw), np.cos(-yaw), 0],
            [0,0,1]])

        self.reldist_relframe = M@reldist_absframe

    def move_fish(self):
        '''Função principal de movimentação do carrinho, aqui os dados recebidos pelos subscribers dos tópicos de localização são trabalhados de forma
        a movimentar o carrinho conforme o desejado e no final a nova velocidade é publicada no tópico /cmd_vel'''
        new_speed = Twist() 
        #define a mensagem a ser publicada como do tipo Twist
        while not rospy.is_shutdown():

            self.set_reldist_relframe() 
            #Chama a função de troca de frame de referencia para trabalharmos com os dados do sonar

            dist = (self.reldist_relframe[0][0]**2 + self.reldist_relframe[1][0]**2 + self.reldist_relframe[2]**2)**.5 
            #Distância em módulo entre os dois carrinhos 

            if dist>0: 
            #garante que a variavel dist não será considerada caso seja inicializada como 0
                if dist >= 4: 
                    new_speed.linear.x = -self.reldist_relframe[0][0]/dist*3
                    new_speed.linear.y = -self.reldist_relframe[1][0]/dist*3
                    new_speed.linear.z = 0.0

                    new_speed.angular.x = 0.0
                    new_speed.angular.y = 0.0
                    new_speed.angular.z = 4.0
                    #Enquanto Marlin estiver a uma certa distância de Nemo, da qual não conseguiria usar a câmera ele 
                    #se guia pelo radar
                elif dist <= 1.5: 
                    new_speed.linear.x = 0.0
                    new_speed.linear.y = 0.0
                    new_speed.linear.z = 0.0

                    new_speed.angular.x = 0.0
                    new_speed.angular.y = 0.0
                    new_speed.angular.z = 0.0
                    #Se chegar muito perto de Nemo, Marlin deve parar para evitar uma possível colisão
                else:
                    if self.is_seeing == True:  
                        new_speed.linear.x = 0.0 
                        new_speed.linear.y = 2.0 
                        new_speed.linear.z = 0.0

                        new_speed.angular.x = 0.0
                        new_speed.angular.y = 0.0
                        new_speed.angular.z = -8 * self.position 
                        '''Caso seja confirmada a presença de Nemo no range da câmera, informação cedida pelo validador,
                        Marlin gira no eixo z de forma a centralizar o bastão na camera. A velocidade é controlada pela variavel
                        self.position que adquire valores cada vez menores conforme o bastão se aproxima do centro, até zerar.
                        Enquanto tenta centralizar o bastão Marlin segue em frente de forma a ajustar seu caminho com base na posição
                        do bastão na câmera, melhor explicado no arquivo "position_and_validation.py".
                        ''' 
                    else:                      
                        new_speed.linear.x = 0.0
                        new_speed.linear.y = 0.0
                        new_speed.linear.z = 0.0

                        new_speed.angular.x = 0.0
                        new_speed.angular.y = 0.0
                        new_speed.angular.z = 4.0
                        #Caso Marlin esteja relativamente perto de Nemo, mas a camêra não veja o bastão verde,
                        #Marlin vai girar até encontra-lo ou até Nemo se afastar e o movimento voltar a ser baseado no sonar
            else:
                new_speed.linear.x = 0.0
                new_speed.linear.y = 0.0
                new_speed.linear.z = 0.0

                new_speed.angular.x = 0.0
                new_speed.angular.y = 0.0
                new_speed.angular.z = 0.0
            #caso a variavel dist se inicialize em 0 o carrinho não se mexe de forma a evitar conflitos
            self.pub_marlin_vel.publish(new_speed)
            #A mensagem da nova velocidade é publicada ao fim de cada loop

if __name__ == '__main__':
    try:
        rospy.init_node('mover', anonymous=True) #Inicializa o nó de movimento
        marlin = Fish_mover() #Instancia a classe Fish_mover()
        marlin.move_fish() #O objeto marlin inicia seu movimento

    except rospy.ROSInterruptException:
        pass
