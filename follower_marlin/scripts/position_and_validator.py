#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import Bool, Float64
import numpy as np
from cv_bridge import CvBridge

class computation():
    '''classe para a computação do valor da posição do bastão verde na câmera e do validador se está vendo ou não'''
    def __init__(self):
        '''Construtora inicializa todos os Publishers e Subscribers e variaveis definidas nos callbacks'''
        self.bridge = CvBridge()
        self.sub = rospy.Subscriber('processed_image', Image, self.callback)
        self.pub_validator = rospy.Publisher('robot_validator', Bool, queue_size=10)
        self.pub_position = rospy.Publisher('position_on_camera', Float64, queue_size=10)
    
    def callback(self, msg):

        mask = self.bridge.imgmsg_to_cv2(msg, "mono8") #transforma a msg passada do tipo Image para numpy.ndarray, mais facilmente utilizável 

        position = Float64()#inicializa as variaveis que será publicada
        validator = Bool()

        result = np.where(mask == 255)#guarda os valores x e y dos píxeis em que a imagem possuí valor 255 (branco); np.where() também é mais eficiente do que iterar sobre todos os elementos com 2 for
        count = len(result[0])#guarda o númere de píxeis com valor 255 (branco)

        if count>0: #só faz a conta se count>0 pois se não poderia acontecer uma divisão por 0
            position.data = sum(result[1])/(count*msg.width)-0.5 #calcula a distancia proporcional no eixo x do centro da imagem
        else:
            position.data = 0 #se count for 0 significa que não está vendo nada, ou seja, a posição não será utilizada

        count = (count/(msg.height * msg.width))**.5 # transforma count em um valor proporcional ao tamanho da imagem

        if count >= 0.1: #limiar arbitrário para saber se está vendo ou não; se tiver poucos píxeis com 255 não considera que está vendo
            validator.data = True
        else:
            validator.data = False
        
        self.pub_position.publish(position)#publica position
        self.pub_validator.publish(validator)#publica validator
        

if __name__ == '__main__':
    try:
        rospy.init_node('computator', anonymous=True)
        c = computation()
        rospy.spin()
    
    except rospy.ROSInterruptException:
        pass