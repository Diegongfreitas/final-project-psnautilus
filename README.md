This is the Final Project of the UFRJ Nautilus Selective Process. 
It's a Project made by me and @jpscavalcanti.

The idea is to make a controlable robot car follow another one.

To make things easier we called the controlable Marlin and the one that must be followed Nemo.

At the star we didn't have access to the code that controls Nemo.

Our job was to make Marlin follow Nemo, no matter which route the second one took.

The gazebo simulation world, models and some topics were provided by the UFRJ Nautilus Workteam.

**Aos membros da Nautilus**

A ordem dos comandos à serem executados no terminal para rodar toda a simulação é a seguinte:

roslaunch nemo_simulator gazebo.launch

rosbag play nemo_vel_1.bag (na pasta do arquivo nemo_vel_1)

roslaunch follower_marlin marlin.launch

Favor, conferir se os arquivos "distance2nemo.py, image_processor.py e position_and_validator.py" estão com permissão de execução.
Caso contrário por favor execute o comando chmod +x "arquivo".py
Ex: chmod +x distance2nemo.py


